#!/usr/bin/env python2

import argparse

from image_operations import ImageOperations

def main():
	imageops = ImageOperations()

	inputfile = args.input[0]

	if args.transf:
		transf = args.transf[0]
		if transf == 'linear' or transf == 'log' or transf == 'sqrt' or transf == 'square' or transf == 'exp' :
			inputfile = imageops.transformation(inputfile, transf)
		else :
			print('Invalid transformation!')
	if args.limiar:
		lim = args.limiar[0]
		if lim <= 1.0 and lim >=0.0 :
			inputfile = imageops.limiarizacao(inputfile, lim)
		else :
			print('Invalid limiar value!')
	if args.eq:
		eq_type = args.eq[0]
		if eq_type == "fdp":
			if args.eq[1]:
				radius = args.eq[1]
				inputfile = imageops.local_equalization(inputfile,radius)
		elif eq_type == "fdpa":
			inputfile = imageops.equalization(inputfile)
		else :
			print('Invalid equalization type!')
	if args.conv:
		conv_type = args.conv[0]
		if conv_type == 'blur' or conv_type == 'sharpen' or conv_type == 'borders' or conv_type == 'detect_borders' or conv_type == 'emboss':
			inputfile = imageops.convolution(inputfile, conv_type)
		elif conv_type == 'high_boost':
			inputfile = imageops.high_boost(inputfile)
		else:
			print('Invalid convolution type!')
	if args.edge:
		filter = args.edge[0]
		inputfile = imageops.edge_detection(inputfile, filter)

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='PIM')
	parser.add_argument('-i', dest='input', action='store', nargs=1, help='Input file')
	parser.add_argument('-t', dest='transf', action='store', nargs=1, help='Transformation: linear|log|sqrt|square|exp')
	parser.add_argument('-l', dest='limiar', action='store', nargs=1, type=float, help='Thresholding: k [0,1]')
	parser.add_argument('-e', dest='eq', action='store', nargs='+', help='Equalization: fdp|fdpa. If fdp is set, need to inform the radius')
	parser.add_argument('-c', dest='conv', action='store', nargs=1, help='Convolution: blur|sharpen|borders|detect_borders|emboss|high_boost')
	parser.add_argument('-ed', dest='edge', action='store', nargs=1, help='Edge detection')

	args = parser.parse_args()

	main()
