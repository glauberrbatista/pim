from PIL import Image
import numpy as np
import matplotlib.image as mpimg
from mpl_toolkits.mplot3d import Axes3D
from pylab import *
from scipy import signal as sg
from scipy.ndimage import filters as fil

class ImageOperations(object):
	def linear(self, f, fmax, fmin):
		return 255/(fmax-fmin)*(f-fmin) + 10

	def log(self, f, fmax, fmin):
		return int(255/np.log10(1+fmax)*log10(f+1))

	def sqrt(self, f, fmax, fmin):
		return int(255/np.sqrt(fmax)*np.sqrt(f))

	def exp(self, f, fmax, fmin):
		return int(255/(np.exp(fmax)-1)*(np.exp(f)-1))

	def square(self, f, fmax, fmin):
		return int(255/(fmax)*f**2)

	def get_bounds(self, image, l, h):
		pixels = [image.getpixel((i,j)) for i in range(l) for j in range(h)]
		return max(pixels), min(pixels)

	def transformation(self, inputfile, transf_type):
		transf_types = {
			"linear" : self.linear,
			"log" : self.log,
			"sqrt" : self.sqrt,
			"exp" : self.exp,
			"square": self.square,
		}

		image = Image.open(inputfile)
		l, h = image.size
		out = Image.new(image.mode, (l,h))

		fmax, fmin = get_bounds(image, l, h)
		for i in range(l):
			for j in range(h):
				out.putpixel((i,j), transf_types[transf_type](image.getpixel((i,j)), fmax, fmin))

		outputfile = inputfile.split('.')[0]+'_transf_'+transf_type+'.jpg'
		out.save(outputfile, 'JPEG')

		image.show()
		out.show()
		return outputfile
		
	def norm(self, ar):
		return 255.*np.absolute(ar)/np.max(ar)

	def limiarizacao(self, inputfile, lim):
		image = Image.open(inputfile)
		l, h = image.size
		out = Image.new(image.mode, (l,h))

		min, max = get_bounds(image, l, h)
		for i in range(l):
			for j in range(h):
				pnorm = (image.getpixel((i,j))-min)/float((max-min))
				if pnorm >= lim:
					out.putpixel((i,j), 0)
				else:
					out.putpixel((i,j), 255)

		outputfile = inputfile.split('.')[0]+'_lim_'+str(lim)+'.jpg'
		out.save(outputfile, 'JPEG')

		out.show()
		image.show()

		return outputfile

	def plot_histogram(self, img, w, h):
		n = img.histogram()

		narray = np.asarray(n)
		p = np.float32(narray)/(w*h)
		b2 = plt.bar(range(256), p)
		plt.show() #param block=False creates a non blocking window, but it closes as soon as it opens

	def equalization(self, inputfile):
		image = Image.open(inputfile)

		w, h = image.size

		out = Image.new(image.mode, (w,h)) #output image
		L = 2**image.bits

		#histogram
		H = [0.0]*L
		for x in range(0, w-1):
			for y in range (0,h-1):
				H[image.getpixel((x,y))] = H[image.getpixel((x,y))] + 1

		#accumulated histogram
		for x in range (1,L):
			H[x] = H[x] + H[x-1]
		#print H

		#histogram normalization
		for x in range (0,L):
			H[x] = H[x]/(w*h) 
		#print H

		#accumulated normalized histogram
		for x in range (0,L):
			H[x] = round(H[x]*L , 2) 
		#print H

		#generate image
		for x in range(0,w):
			for y in range(0,h):
				aux = H[image.getpixel((x,y))]
				out.putpixel((x,y), aux)

		outputfile = inputfile.split('.')[0]+'_eq_fdpa.jpg'
		out.save(outputfile, 'JPEG')

		#plot the original image histogram
		plot_histogram(image, w, h)
		#plot the equalized histogram
		plot_histogram(out, w, h)
		image.show()
		out.show()

		return outputfile

	def local_equalization(self, inputfile, radius):
		radius = int(radius)
		image = Image.open(inputfile)
		w, h = image.size
		out = Image.new(image.mode, (w,h)) #output image
		L = 2**image.bits

		for i in range (radius, w-radius):
			for j in range(radius, h-radius):
				H = [0]*L
				for x in range(i-radius, radius+i+1):
					for y in range(j-radius, radius+j+1):
						H[image.getpixel((x,y))] = H[image.getpixel((x,y))] + 1
				for x in range (1,256):
					H[x] = H[x] + H[x-1]
				tones = ((radius*2)+1)**2
				aux = H[image.getpixel((i,j))]
				out.putpixel((i,j), ((255)/tones)*aux)

		outputfile = inputfile.split('.')[0]+'_eq_fdp.jpg'
		out.save(outputfile, 'JPEG')

		#plot the original image histogram
		plot_histogram(image, (w,h))
		#plot the equalized histogram
		plot_histogram(out, (w,h))
		image.show()
		out.show()

		return outputfile

	def convolution(self, inputfile, conv_type):
		blur_matrix =  [[1,1,1], 
						[1,1,1], 
						[1,1,1]]

		sharpen_matrix = [[0,-1,0], 
						[-1,5,-1], 
						[0,-1,0]]

		borders_matrix = [[0,0,0], 
						[-1,1,0], 
						[0,0,0]]

		detect_borders_matrix =  [[0,1,0], 
						[1,-4,1], 
						[0,1,0]]

		emboss_matrix = [[-2,-1,0], 
						[-1,1,1], 
						[0,1,0]]

		conv_types = {
			"blur" : blur_matrix,
			"sharpen" : sharpen_matrix,
			"borders" : borders_matrix,
			"detect_borders" : detect_borders_matrix,
			"emboss" : emboss_matrix,
		}

		image = Image.open(inputfile)
		ar = np.asarray(image, dtype=np.float32)

		ar = norm(sg.convolve(ar, conv_types[conv_type]))
		out = Image.fromarray(ar.round().astype(np.uint8))

		outputfile = inputfile.split('.')[0]+'_conv_'+conv_type+'.jpg'
		out.save(outputfile, 'JPEG')

		image.show()
		out.show()

		return outputfile

	def edge_detection(self, inputfile, filter):
		prewitt_x = [[-1,0,1],
					[-1,0,1],
					[-1,0,1]]
		prewitt_y = [[-1,-1,-1],
					[0,0,0],
					[1,1,1]]

		image = Image.open(inputfile)
		l, h = image.size
		out = Image.new(image.mode, (l,h))

		#Apply filter
		for i in range(l-1):
			for j in range(h-1):
				area = [image.getpixel((((i+x)%l, (j+y)%h))) for x in range(-1,2) 
											for y in range(-1,2)
											if i+x >= 0 and j+y >= 0]
				value = image.getpixel((i,j))
				if filter == 'mean':
					value = int(np.mean(area))
				elif filter == 'median':
					area.sort()
					value = area[int(len(area)/2)]
				out.putpixel((i,j), value)

		ar = np.asarray(out, dtype=np.float32)
		
		gx = sg.convolve(ar, prewitt_x)
		gy = sg.convolve(ar, prewitt_y)
	
		G = np.sqrt(gx**2 + gy**2)
		#D = np.arctan2(gx, gy)

		edge_out = Image.fromarray(G.round().astype(np.uint8))
		outputfile = inputfile.split('.')[0]+'_edge_'+filter+'.jpg'
		edge_out.save(outputfile, 'JPEG')

		return outputfile

	def high_boost(self, inputfile):
		image = Image.open(inputfile)
		l, h = image.size
		blur = Image.new(image.mode, (l,h))

		#Apply filter
		for i in range(l-1):
			for j in range(h-1):
				area = [image.getpixel((((i+x)%l, (j+y)%h))) for x in range(-1,2) 
											for y in range(-1,2)
											if i+x >= 0 and j+y >= 0]
				value = int(np.mean(area))
				blur.putpixel((i,j), value)

		image_ar = np.asarray(image, dtype=np.float32)
		blur_ar = np.asarray(blur, dtype=np.float32)
		g_mask = image_ar - blur_ar

		G = image_ar + 1.0*g_mask
		sharp_out = Image.fromarray(G.round().astype(np.uint8))
		outputfile = inputfile.split('.')[0]+'_sharp.jpg'

		sharp_out.save(outputfile, 'JPEG')

		return outputfile
		
